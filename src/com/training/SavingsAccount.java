package com.training;

public class SavingsAccount extends Account{
    private double balance;
    private String name;
    public SavingsAccount(String name, double balance){
        super.balance = balance;
        super.name = name;
    }

    @Override
    public void addInterest() {
        setBalance(getBalance() *  1.4);
    }
}
