package com.training;

public class TestInheritance {
    public static void main(String[] args) {
        Account[] myAccount = new Account[3];
        SavingsAccount account = new SavingsAccount("Pavi",2);
        SavingsAccount savingsAccount = new SavingsAccount("Kavi",4);
        CurrentAccount currentAccount =new CurrentAccount("Shavi",6);

        myAccount[0] = account;
        myAccount[1] = savingsAccount;
        myAccount[2] = currentAccount;

        for (int i = 0; i < 3 ; i++) {
            if(myAccount[i] !=null) {
                myAccount[i].addInterest();
                System.out.println("Name "+myAccount[i].getName());
                System.out.println("Balance "+ myAccount[i].getBalance());
            }

        }

    }
}
