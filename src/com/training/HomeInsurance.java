package com.training;

public class HomeInsurance implements Detailable{
    private double amountInsured;
    private double premium;
    private double excess;

    public HomeInsurance()
    {

    }
    public HomeInsurance(double amountInsured, double premium, double excess)
    {
        this.amountInsured = amountInsured;
        this.premium = premium;
        this.excess = excess;
    }

    @Override
    public String getDetails() {
        return  "" + premium + " " + excess;
    }
}
