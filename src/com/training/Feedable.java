package com.training;

public interface Feedable {

    void feed();

}
