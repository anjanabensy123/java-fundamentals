package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class Main {

    public static void main(String[] args) {

        Pet myPet = new Pet();
        myPet.feed();
        myPet.setName("Dora");
        myPet.setNumlegs(4);
        System.out.println("PetName "+myPet.getName());
        System.out.println("PetLegs "+myPet.getNumlegs());

        System.out.println("**************");
        Dragon myDragon = new Dragon();
        myDragon.setName("Mycroft");
        myDragon.setNumlegs(8);
        System.out.println("PetName "+myDragon.getName());
        System.out.println("PetLegs "+myDragon.getNumlegs());
        myDragon.feed();
        myDragon.breatheFire();

        Pet peto;
        peto = myDragon;
        System.out.println("Exprimentla");
        peto.feed();
        System.out.println("this val"+peto.getNumlegs());


        System.out.println("**************");
        Pet secondDragon = new Dragon();
        secondDragon.setName("Mycroft");
        secondDragon.setNumlegs(8);
        System.out.println("PetName "+secondDragon.getName());
        System.out.println("PetLegs "+secondDragon.getNumlegs());
        secondDragon.feed();

        Pet firstpet  = new Pet();
        firstpet.setName("Meow");
        Pet[] petarray =  new Pet[4];
        petarray[0] = firstpet;
        System.out.println("com.training.pets.Pet array"+petarray[0]);
        petarray[0].feed();

        petarray[1] = myDragon;
        petarray[1].feed();
    }

}
