package com.training.pets;

public class Pet {

    private String name;
    private int numlegs;

    public void feed(){
        System.out.println("Feed generic "+name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumlegs(int numlegs) {
        this.numlegs = numlegs;
    }

    public String getName() {
        return name;
    }

    public int getNumlegs() {
        return numlegs;
    }
}
