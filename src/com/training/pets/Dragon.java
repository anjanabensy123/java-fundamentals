package com.training.pets;

import com.training.Feedable;

public class Dragon extends Pet implements Feedable {

    public Dragon(String name){
        this.setName("Myname");
        this.setNumlegs(4);
    }

    public Dragon() {

    }


    public void feed(){
        System.out.println("Feed the dragon some locust");
    }
    public void breatheFire(){
        System.out.println("FIREEEEEE");
    }
}
