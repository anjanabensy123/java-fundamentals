package com.training;

public class CurrentAccount extends Account{
    private double balance;
    private String name;
    public CurrentAccount(String name, double balance){
        super.balance = balance;
        super.name = name;
    }

    @Override
    public void addInterest() {
        setBalance(getBalance() *  1.1);
    }
}
