package com.training;

import com.training.person.Person;
import com.training.pets.Dragon;

public class InterfacesMain {
    public static void main(String[] args) {
        Feedable[] thingsWeCanFeed = new Feedable[5];
        Person horace = new Person("Horace");
        horace.setName("Horace xyz");

        Dragon hor = new Dragon("Hor");
        hor.setName("Hor xyz");
        hor.breatheFire();

        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[1] = hor;

        for (int i = 0; i <thingsWeCanFeed.length ; i++) {
            if(thingsWeCanFeed[i]!=null){
                thingsWeCanFeed[i].feed();
            }
            if (thingsWeCanFeed[i] instanceof Dragon){
                ((Dragon) thingsWeCanFeed[i]).breatheFire();
            }

        }

    }
}
