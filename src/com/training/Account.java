package com.training;

public abstract class Account implements Detailable {
    protected double balance;
    protected String name;

    public Account(){

    }
    public Account(String name,double balance)
    {
        this.name = name;
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String getDetails() {
        return  "" + name + " " + balance;
    }

    public abstract void addInterest();
}
